from sre_constants import SUCCESS
from flask import Blueprint, jsonify, request, redirect, url_for
import sqlite3
import DB as DB
import os

api_blueprint = Blueprint('api', __name__)

current_directory = os.path.dirname(os.path.abspath(__file__))
db_file_path = os.path.join(current_directory, 'databaze.db')
conn = sqlite3.connect(db_file_path)

disp_range = 0
logged = False
logged_as = "nobody"

@api_blueprint.route('/api/measures', methods=['POST'])
def create_measure():
    new_measure = request.json
    global db_file_path
    conn = sqlite3.connect(db_file_path)
    DB.addMeasurement(conn, new_measure["temp"])
    return jsonify(new_measure), 201

@api_blueprint.route('/api/range/<int:range>', methods=['DELETE'])
def delete_range(range):
    global measures
    global db_file_path
    conn = sqlite3.connect(db_file_path)
    if (range > 0 and range <= DB.len(conn, "measurements")):
        DB.deleteRange(conn, range)
        return jsonify({"message": f"Deleted first {range} measurements"}), 200
    else:
        return jsonify({"error": "Invalid delete count"}), 400

@api_blueprint.route('/api/range/<int:range>', methods=['GET'])
def display_range(range):
    global disp_range, db_file_path
    conn = sqlite3.connect(db_file_path)
    if (range > 0 and range <= DB.len(conn, "measurements")):
        disp_range = range
        return jsonify({"message": f"Displaying last {range} measurements"}), 200
    else:
        return jsonify({"error": "Invalid display count"}), 400

@api_blueprint.route('/api/register', methods=['POST'])
def add_user():
    new_user = request.json
    global db_file_path
    conn = sqlite3.connect(db_file_path)
    if (DB.countUsers(conn, new_user["name"]) == 0):
        if(new_user["password"] == new_user["passwordAgain"]):
            DB.addUser(conn, new_user["name"], new_user["password"])
            return jsonify(new_user), 201
        else:
            return jsonify({"error": "Passwords don't match"}), 400
    else:
        return jsonify({"error": "Name already taken"}), 400
    

@api_blueprint.route('/api/login', methods=['POST'])
def login_user():
    user = request.json
    global db_file_path
    conn = sqlite3.connect(db_file_path)
    success = DB.loginUser(conn, user["name"], user["password"])
    if success:
        global logged_as, logged
        logged = True
        logged_as = user["name"]
        return redirect(url_for("dashboard", name = user["name"]))
    else:
        return jsonify({"error": "Name and password do not match"}), 400

@api_blueprint.route('/api/logout')
def logout():
    global logged, logged_as
    logged = False
    logged_as = ""
    return redirect(url_for("login"))