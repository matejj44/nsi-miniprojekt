import sqlite3
import serial
import json
import DB as DB
from threading import Thread
from api_routes import db_file_path

def UART():
    try:
        ser = serial.Serial('/dev/cu.usbserial-14342410', 115200)
        conn = sqlite3.connect(db_file_path)

    except Exception as e:
        print(e)
        print("unable to open COM port")
        exit(-1)

    while True:
        line = ser.readline().decode().strip()
        data = json.loads(line)
        value = data["value"]
        print("From ESP adding measurement: ", value)
        DB.addMeasurement(conn, value)

def thread():
    reading = Thread(target=UART)
    reading.daemon = True
    reading.start()