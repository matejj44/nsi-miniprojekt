from flask import Flask, render_template, redirect, url_for
from api_routes import api_blueprint, db_file_path
import sqlite3
import DB as DB
import Read as Read

app = Flask(__name__)

app.register_blueprint(api_blueprint)

#DIFF APP
conn = sqlite3.connect(db_file_path)

DB.createDB(conn)

for i in range(5):
    DB.addMeasurement(conn, (19.5+i))

DB.addUser(conn, "admin", "admin")

conn.commit()

Read.thread()

@app.route('/')
def index():
    return render_template('base.html')

@app.route('/login')
def login():
    from api_routes import logged, logged_as
    if logged:
        return redirect(url_for('dashboard', name = logged_as))
    return render_template("login.html")

@app.route('/register')
def register():
    return render_template("register.html")

@app.route('/<name>')
def dashboard(name=""):
    from api_routes import disp_range, logged
    if logged:
        conn = sqlite3.connect(db_file_path)
        measurements_table = DB.displayRangeHTMLTable(conn, -1)
        display_table = DB.displayRangeHTMLTable(conn, disp_range)
        return render_template("dashboard.html", name = name, measurements_table=measurements_table, display_table=display_table)
    else:
        return redirect(url_for('login'))

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5000, debug=True, use_reloader = False)