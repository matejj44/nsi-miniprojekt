from machine import Timer
import time
import urandom
import ujson
import os

timer = Timer(-1)

def generate(timer):
    value=urandom.getrandbits(5)
    print(ujson.dumps({"value": value}))

timer.init(period=30000, mode=Timer.PERIODIC, callback=generate)

while True:
    pass