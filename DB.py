import hashlib
import datetime

max_id = 0

def get_id(conn, id, table_name):
    cur = conn.cursor()
    cur.execute(f"SELECT MAX({id}) FROM {table_name}")
    max_id = cur.fetchone()[0]
    if max_id:
        max_id += 1
    else:
        max_id = 1
    return max_id

def createDB(conn):
    cur = conn.cursor()
    cur.execute("DROP TABLE IF EXISTS measurements;")
    cur.execute("DROP TABLE IF EXISTS users;")
    SQL = """
    CREATE TABLE measurements(
        meas_id         INTEGER PRIMARY KEY AUTOINCREMENT,
        timestamp       TIMESTAMP,
        value           FLOAT
    );
    """
    cur.execute(SQL)
    SQL = """
    CREATE TABLE users (
        user_id         INTEGER PRIMARY KEY AUTOINCREMENT,
        name            VARCHAR(100),
        password        VARCHAR(100)
    );
    """
    cur.execute(SQL)
    conn.commit()

def addUser(conn, name, password):
    cur = conn.cursor()
    hashed_password = hashlib.sha256(password.encode()).hexdigest()
    SQL = f"""
    INSERT INTO users(user_id, name, password)
    VALUES (
        {get_id(conn, "user_id", "users")},
        "{name}",
        "{hashed_password}"
    );
    """     
    cur.execute(SQL)
    conn.commit()
    cur.close()

def addMeasurement(conn, value):
    cur = conn.cursor()
    localized_time = datetime.datetime.utcnow() + datetime.timedelta(hours=2)
    formatted_time = localized_time.strftime('%d.%m.%Y %H:%M')
    SQL = f"""
    INSERT INTO measurements(meas_id, timestamp, value)
    VALUES (
        {get_id(conn, "meas_id", "measurements")},
        "{formatted_time}",
        {value}
    );
    """     
    cur.execute(SQL)
    conn.commit()
    cur.close()

def getMeasurement(conn, meas_id):
    cur = conn.cursor()
    cur.execute(f"SELECT * FROM measurements WHERE meas_id={meas_id};")
    ret = cur.fetchall()
    cur.close()
    return ret

def displayRangeHTMLTable(conn, range):
    cur = conn.cursor()
    if range == -1:
        cur.execute("SELECT * FROM measurements")
        rows = cur.fetchall()
    else:
        total_rows = len(conn, "measurements")
        cur = conn.cursor()
        start_row_index = max(0, total_rows - range)
        cur.execute(f"SELECT * FROM measurements ORDER BY ROWID LIMIT {range} OFFSET {start_row_index}")
        rows = cur.fetchall()

    html_table = "<thead><tr><th>Id</th><th>Time of Measurement</th><th>Temperature</th></tr></thead><tbody>"
    for row in rows:
        html_table += "<tr>"
        for column in row:
            html_table += "<td>{}</td>".format(column)
        html_table += "</tr>"
    html_table += "</tbody>"
    cur.close()
    return html_table

def deleteRange(conn, range):
    cur = conn.cursor()
    query = "DELETE FROM measurements WHERE rowid IN (SELECT rowid FROM measurements LIMIT {})".format(range)
    cur.execute(query)
    conn.commit()
    cur.close()

def len(conn, table_name):
    cur = conn.cursor()
    query = ("SELECT COUNT(*) FROM {}".format(table_name))
    cur.execute(query)
    row_count = cur.fetchone()[0]
    cur.close()
    return row_count

def countUsers(conn, name):
    cur = conn.cursor()
    cur.execute("SELECT COUNT(*) FROM users WHERE name = ?", (name,))
    count = cur.fetchone()[0]
    cur.close()
    return count

def loginUser(conn, name, password_entered):
    cur = conn.cursor()
    hashed_password = hashlib.sha256(password_entered.encode()).hexdigest()
    cur.execute("SELECT password FROM users WHERE name = ?", (name,))
    password_real = cur.fetchone()[0]
    cur.close()
    if(hashed_password == password_real):
        return True
    else:
        return False